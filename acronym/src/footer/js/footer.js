const move_tick = 20;
var mouse_x = 0;
var mouse_y = 0;

/**
 * Подключение скриптов без html
 * 
 * @author ShchMax
 * @file footer
 * @function
 * @param {string} src Путь к подключаемому модулю
 * @param {function} callback Функция callback
 */
function loadScripts( src, callback ) {
	var script = document.createElement("SCRIPT"),
	head = document.getElementsByTagName( "head" )[ 0 ],
	error = false;
	script.type = "text/javascript";
	script.onload = script.onreadystatechange = function( e ){
		if ( ( !this.readyState || this.readyState == "loaded" || this.readyState == "complete" ) ) {
			if ( !error ) {
				removeListeners();
				callback( true );
			} else {
				callback( false );
			}
		}
	};
	script.onerror = function() {
		error = true;
		removeListeners();
		callback( false );
	}
	function errorHandle( msg, url, line ) {
		if ( url == src ) {
			error = true;
			removeListeners();
			callback( false );
		}
		return false;
	}
	function removeListeners() {
		script.onreadystatechange = script.onload = script.onerror = null;
		if ( window.removeEventListener ) {
			window.removeEventListener('error', errorHandle, false );
		} else {
			window.detachEvent("onerror", errorHandle );
		}
	}
	if ( window.addEventListener ) {
		window.addEventListener('error', errorHandle, false );
	} else {
		window.attachEvent("onerror", errorHandle );
	}
	script.src = src;
	head.appendChild( script );
}
loadScripts('http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', function( status ){});
loadScripts('../../bower_components/howler.js/dist/howler.js', function( status ){});

document.addEventListener('mousemove', function(event) {
	mouse_x = event.pageX;
	mouse_y = event.pageY;
});

let timer_for_footer_bar = setInterval(() => {
	var margin_top = +($('footer').css('margin-top').slice(0, -2));
	if (mouse_y >= 570) {
		if (margin_top > -30) {
			margin_top--;
			$('footer').css('margin-top', (margin_top - 1) + "px");
		}
	} else {
		if (margin_top < 0) {
			$('footer').css('margin-top', (margin_top + 1) + "px");
		}
	}
}, move_tick);

/**
 * Смена громкости
 * 
 * @author ShchMax
 * @file footer
 * @function
 */
function change_volume() {
	var vol_input = document.getElementById("sound_volume");
	Howler.volume(vol_input.value);
}