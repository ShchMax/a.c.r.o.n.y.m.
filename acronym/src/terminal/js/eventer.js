var text = ["System are loaded","Please, type help to get main commands."].concat(execute(""));
var is_seen = false;
var left_marg = 0;
var top_marg = 0;
var cur = [text.length - 1, 0];
var screen_width = 61;
var screen_height = 17;
var death = text[text.length - 1].length + 1;

/**
 * Функция возвращает строку, которая должна быть на экране
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @param {number} raw Номер строки на экране
 * @return {string}
 */
function get_screen_raw(raw) {
	var ans = "";
	for (var i = left_marg; i < (left_marg + screen_width); ++i) {
		if (text.length <= top_marg + raw) {
			return "";
		}
		if (text[top_marg + raw].length <= i) {
			return ans;
		}
		ans += text[top_marg + raw][i];
	}
	return ans;
}

/**
 * Возвращает весь текст, который нужно вывести на экран
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @return {string}
 */
function get_screen_raws() {
	var ans = [];
	for (var i = 0; i < screen_height; ++i) {
		ans.push(get_screen_raw(i));
	}
	return ans;
}

/**
 * Возвращяет координаты курсора
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @return {[number, number]}
 */
function get_cur() {
	var g = is_seen;
	is_seen = false;
	return [cur, g];
}

/**
 * Проверяет, является ли нажатая клавиша(а точнее её ключ) печатаемым символом
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @param {string} с Ключ клавиши
 * @return {boolean}
 */
function isPrintable(c) {
	if (c.length > 1) {
		return false;
	}
	return (c >= ' ') && (c <= '~');
}

/**
 * Двигает курсор влево
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function move_cur_left() {
	if (cur[1] + left_marg <= death) {
		cur[1] = death - left_marg;
	}
	if (cur[1] == 0) {
		left_marg = Math.max(left_marg - 1, 0);
	} else {
		cur[1]--;
	}
}

/**
 * Двигает курсор вправо
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function move_cur_right() {
	if (left_marg + cur[1] >= text[top_marg + cur[0]].length) {
		cur[1] = text[top_marg + cur[0]].length - left_marg;
		return;
	}
	if (cur[1] == (screen_width - 1)) {
		left_marg += 1;
	} else {
		cur[1]++;
	}
}

/**
 * Создаёт новую строку
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function new_line() {
	if (cur[0] == screen_height - 1) {
		top_marg++;
	} else {
		cur[0]++;
	}
	cur[1] = 0;
}

/**
 * Добавляет букву в строку
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @param {string} c Та буква, что нужно добавить
 */
function add_letter(c) {
	if (left_marg + cur[1] >= text[top_marg + cur[0]].length) {
		text[top_marg + cur[0]] += c;
	} else {
		var ans = "";
		for (var i = 0; i < text[top_marg + cur[0]].length; ++i) {
			if (i == left_marg + cur[1]) {
				ans += c
			}
			ans += text[top_marg + cur[0]][i];
		}
		text[top_marg + cur[0]] = ans;
	}
	move_cur_right();
}

/**
 * Убирает букву из строки
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function delete_letter() {
	if (left_marg + cur[1] < death) {
		beep();
	} else {
		var ans = "";
		for (var i = 0; i < text[top_marg + cur[0]].length; ++i) {
			if (i == left_marg + cur[1] - 1) {
				continue;
			}
			ans += text[top_marg + cur[0]][i];
		}
		text[top_marg + cur[0]] = ans;
	}
	move_cur_left();
}

move_cur_left();
document.addEventListener('keydown', function(event) {
	var p = event.key;
	if (isPrintable(p)) {
		add_letter(p);
		is_seen = true;
		return;
	}
	if (p == "ArrowLeft") {
		is_seen = true;
		move_cur_left();
	}
	if (p == "ArrowRight") {
		is_seen = true;
		move_cur_right();
	}
	if (p == "Backspace") {
		is_seen = true;
		delete_letter();
	}
	if (p == "Enter") {
		is_seen = true;
		var add = execute(text[top_marg + cur[0]].substr(death - 1));
		for (var i = 0; i < add.length; ++i) {
			new_line();
		}
		text = text.concat(add);
		death = text[text.length - 1].length + 1;
		move_cur_left();
	}
});