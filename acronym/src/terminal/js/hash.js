var fft = require(`fft-js`).dft;

/**
 * Функция возвращает хэшированную строку
 * 
 * @author ShchMax
 * @file teerminal
 * @function
 * @param {string} a Сама строка
 * @return {string}
 */
function hash(a) {
	var b = [];
	for (var i = 0; i < a.length; i++) {
		b.push(a.charCodeAt(i));
	}
	b = fft(b);
	var c = [];
	b.forEach((elem) => {
		c.push(Math.abs(Math.round(elem[0])) % 96 + 32);
	});
	var ans = "";
	c.forEach((elem) => {
		ans += String.fromCharCode(elem);
	});
	return ans;
}