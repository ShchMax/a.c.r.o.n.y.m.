/**
 * Выход в меню
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function exit_menu() {
	ipcRenderer.send('menu');
}