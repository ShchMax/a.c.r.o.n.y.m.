const {ipcRenderer} = require('electron');

var endl = `>>>$ `;
var now = root;
var path = [root];
var sudo_request = false;
var is_sudo = false;

/**
 * Данная функция возвращает ошибку количества аргументов
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @param {string} name Название комманды
 * @param {number} need Сколько должно быть аргументов
 * @param {number} has Количество аргументов включая комманду
 * @return {Array.<string>}
 */
function arg_err(name, need, has) {
	has--;
	if (need == 0) {
		return [`${name} expects no arguments, but given ${has}`, endl];
	}
	return [`${name} expects ${need} argument, but given ${has}`, endl];
}

/**
 * Данная функция выполняет строку комманд
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @param {string} s Комманда, которую надо выполнить
 * @return {Array.<string>}
 */
function execute(s) {
	if (sudo_request) {
		sudo_request = false;
		if (hash(s) == pass_hash) {
			is_sudo = true;
			return [endl];
		} else {
			return [`Incorrect password`, endl];
		}
	}

	var pres = s.split(` `);
	s = [];
	pres.forEach((elem) => {
		if (elem !== ``) {
			s.push(elem);
		}
	})
	if (s.length == 0) {
		return [endl];
	}

	if (s[0] == `sudo`) {
		if (s.length > 1) {
			return arg_err(`sudo`, 0, s.length);
		}
		sudo_request = true;
		return [`Password:`]
	}

	if (s[0] == `exit`) {
		if (s.length > 1) {
			return arg_err(`exit`, 0, s.length);
		}
		ipcRenderer.send(`exit`);
	}


	if (s[0] == `help`) {
		if (s.length > 2) {
			return arg_err(`help`, 1, s.length);
		}
		if (s.length == 1) {
			var ans = [];
			for (var command of helps.keys()) {
				ans.push(command);
			}
			ans.push(endl);
			return ans;
		}
		if (helps.get(s[1]) !== undefined) {
			return helps.get(s[1]).concat([endl]);
		}
		return [`Error`, `Unknown command: ${s[1]}`, endl];
	}


	if (s[0] == `pwd`) {
		if (s.length > 1) {
			return arg_err(`pwd`, 0, s.length);
		}
		var ans = ``;
		path.forEach((elem) => {
			ans += elem.name + `/`;
		});
		return [ans, endl];
	}

	if (s[0] == `ls`) {
		if (s.length > 1) {
			return arg_err(`ls`, 0, s.length);
		}
		return path[path.length - 1].all().concat([endl]);
	}

	if (s[0] == `cd`) {
		if (s.length > 2) {
			return arg_err(`cd`, 1, s.length);
		}
		if (path[path.length - 1].consist_dir(s[1])) {
			if (path[path.length - 1].get_elem(s[1]).can_do() || is_sudo) {
				path.push(path[path.length - 1].get_elem(s[1]));
				return [endl];
			} else {
				return [`Permission denied`, endl];
			}
		}
		if (s[1] == "..") {
			if (path.length == 1) {
				return [endl];
			}
			path.pop();
			return [endl];
		}
		return [`No such directory ${s[1]}`, endl];
	}

	if (s[0] == `md`) {
		if (s.length != 2) {
			return arg_err(`md`, 1, s.length);
		}
		if (path[path.length - 1].consist_dir(s[1])) {
			return [`Dir exists`, endl];
		} else if (path[path.length - 1].consist_file(s[1])) {
			return [`This name already engaged`, endl];
		}
		path[path.length - 1].add(new Dir(s[1]));
		return [endl];
	}

	if (s[0] == `mf`) {
		if (s.length != 2) {
			return arg_err(`mf`, 1, s.length);
		}
		if (path[path.length - 1].consist_file(s[1])) {
			return [`File exists`, endl];
		} else if (path[path.length - 1].consist_dir(s[1])) {
			return [`This name already engaged`, endl];
		}
		path[path.length - 1].add(new File(s[1]));
		return [endl];
	}

	if (s[0] == `zim`) {
		if (s.length == 1) {
			return [`Zim already not installed`, endl];
		}
		if (s.length == 2) {
			return [`Zim already not installed`, endl];
		}
		return arg_err(`zim`, 1, s.length);
	}

	return [`Error`, `Unknown command: ${s[0]}`, endl];
}