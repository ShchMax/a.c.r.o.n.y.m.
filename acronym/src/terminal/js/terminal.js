var canv = document.getElementById("main_canvas");
var c = canv.getContext('2d');
canv.height = 400;
canv.width = 800;
var redraw_timer = 20;
var cnt_of_ticks = 0;
var num_of_ticks = 50;
var cur_symbh = "\u2588";

/**
 * Функция рисующая монитор
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function redraw_monitor() {
	// Leg Fill
	c.beginPath();
	c.moveTo(300, 390);
	c.lineTo(300, 410);
	c.lineTo(500, 410);
	c.lineTo(500, 390);
	c.closePath();
	c.fillStyle = "#1A1A1A";
	c.fill();

	// Leg Stroke
	c.strokeStyle = "#000000";
	c.lineWidth = 4;
	c.beginPath();
	c.moveTo(300, 390);
	c.lineTo(300, 410);
	c.lineTo(500, 410);
	c.lineTo(500, 390);
	c.closePath();
	c.stroke();

	// Monitor Fill
	c.beginPath();
	c.moveTo(4, 29);
	c.arcTo(4, 4, 29, 4, 25);
	c.lineTo(771, 4);
	c.arcTo(796, 4, 796, 29, 25);
	c.lineTo(796, 371);
	c.arcTo(796, 396, 771, 396, 25);
	c.lineTo(29, 396);
	c.arcTo(4, 396, 4, 371, 25);
	c.closePath();
	c.fillStyle = "#2A2A2A";
	c.fill();

	// Monitor Stroke
	c.beginPath();
	c.moveTo(4, 29);
	c.arcTo(4, 4, 29, 4, 25);
	c.lineTo(771, 4);
	c.arcTo(796, 4, 796, 29, 25);
	c.lineTo(796, 371);
	c.arcTo(796, 396, 771, 396, 25);
	c.lineTo(29, 396);
	c.arcTo(4, 396, 4, 371, 25);
	c.lineTo(4, 29);
	c.closePath();
	c.stroke();

	// Screen Fill
	c.beginPath();
	c.moveTo(25, 25);
	c.lineTo(775, 25);
	c.lineTo(775, 375);
	c.lineTo(25, 375);
	c.closePath();
	c.fillStyle = "#282937";
	c.fill();

	// Screen Stroke
	c.beginPath();
	c.moveTo(25, 25);
	c.lineTo(775, 25);
	c.lineTo(775, 375);
	c.lineTo(25, 375);
	c.closePath();
	c.stroke();
}
/**
 * Данная функция пишет строку на мониторе
 * 
 * @author ShchMax
 * @file terminal
 * @function
 * @param {string} text Текст, который необходимо вывести
 * @param {number} raw Строка, на которой должен быть текст
 * @param {number} cur_raw Строка, на которой курсор
 * @param {number} cur_col Столбец, на котором курсор
 */
function draw_text_line(text, raw, cur_raw, cur_col) {
	var top_marg = 25;
	var left_marg = 35;
	var text_w = 20;
	var max_length = 740;
	var new_string = "";
	if (cnt_of_ticks < num_of_ticks / 2) {
		if (cur_raw == raw) {
			for (var i = 0; i < text.length; ++i) {
				if (i == cur_col) {
					new_string += cur_symbh;
				} else {
					new_string += text[i];
				}
			}
			if (cur_col == text.length) {
				new_string += cur_symbh;
			}
			text = new_string;
		}
	}
	c.font = "20px courier";
	c.fillStyle = "#E2E2E4";
	c.fillText(text, left_marg, top_marg + text_w * (raw + 1));
}

/**
 * Данная функция пишет текст на мониторе
 * 
 * @author ShchMax
 * @file terminal
 * @function
 */
function draw_lines() {
	cnt_of_ticks = (cnt_of_ticks + 1) % num_of_ticks;
	var curi = get_cur();
	var cur = curi[0];
	if (curi[1]) {
		cnt_of_ticks = 0;
	}
	var text = get_screen_raws();
	text.forEach((elem, index) => {
		draw_text_line(elem, index, cur[0], cur[1]);
	});
}

let redraw = setInterval(() => {
	redraw_monitor();
	draw_lines();
}, redraw_timer);