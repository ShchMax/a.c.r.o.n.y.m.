/**
 * Класс отвеающий за директорию
 * 
 * @author ShchMax
 * @file terminal
 * @class
 */
class Dir {
	/**
	 * Создаёт директорию
	 * 
	 * @author ShchMax
	 * @file terminal
	 * @constructor
	 * @param {string} namee Имя директории
	 * @param {boolean} prot {Необязательный} Защищена ли директория
	 */
	constructor(namee, prot) {
		this.namee = namee;
		this.dir = new Map();
		this.prot = prot;
	}

	/**
	 * Имя директории
	 * 
	 * @author ShchMax
	 * @method
	 * @return {string}
	 */	
	get name() {
		return this.namee;
	}

	/**
	 * Содержит ли директория данную директорию
	 * 
	 * @author ShchMax
	 * @method
	 * @param {string} smth Имя директории
	 * @return {boolean}
	 */
	consist_dir(smth) {
		return this.dir.has(smth) && this.dir.get(smth).file_type === undefined;
	}

	/**
	 * Содержит ли директория данный файл
	 * 
	 * @author ShchMax
	 * @method
	 * @param {string} smth Имя файла
	 * @return {boolean}
	 */
	consist_file(smth) {
		return this.dir.has(smth) && this.dir.get(smth).file_type !== undefined;
	}

	/**
	 * Добавления объекта в директорию
	 * 
	 * @author ShchMax
	 * @method
	 * @param smth Объект
	 */
	add(smth) {
		this.dir.set(smth.name, smth);
	}

	/**
	 * Возвращает элемент по имени
	 * 
	 * @author ShchMax
	 * @method
	 * @param {string} smth
	 * @return {any}
	 */
	get_elem(smth) {
		return this.dir.get(smth);
	}

	/**
	 * Возвращает защищена ли данная директория
	 * 
	 * @author ShchMax
	 * @method
	 * @return {boolean}
	 */
	can_do() {
		return this.prot === undefined;
	}

	/**
	 * Возвращает имена всего содержимого
	 * 
	 * @author ShchMax
	 * @method
	 * @return {Array.<string>}
	 */	
	all() {
		var ans = [];
		for (var el of this.dir.keys()) {
			ans.push(el);
		}
		return ans;
	}
}

/**
 * Класс отвеающий за файл
 * 
 * @author ShchMax
 * @file terminal
 * @class
 */
class File {
	/**
	 * Создаёт файл
	 * 
	 * @author ShchMax
	 * @file terminal
	 * @constructor
	 * @param {string} namee Имя файла
	 */
	constructor(namee) {
		this.cont = undefined;
		this.namee = namee;
		this.file_type = true;
	}

	/**
	 * Имя файла
	 * 
	 * @author ShchMax
	 * @method
	 * @return {string}
	 */	
	get name() {
		return this.namee;
	}

	/**
	 * Содержимое файла
	 * 
	 * @author ShchMax
	 * @method
	 * @return {string}
	 */	
	get_file() {
		return this.cont;
	}

	/**
	 * Изменяет содержимое файла
	 * 
	 * @author ShchMax
	 * @method
	 * @param {Array.<string>} content Массив из строк файла
	 */
	set_file(content) {
		this.cont = content;
	}
}

var helps = new Map();
var pass_hash = `D#$"$,$"$#`;

helps.set(`sudo`, [`Sudo - gives access to protected files and directories`]);
helps.set(`exit`, [`Exit - exits terminal`]);
helps.set(`help`, [`Help - helping you to get help`]);
helps.set(`pwd`, [`Help - shows path from root`]);
helps.set(`ls`, [`Help - shows the contents of the directory`]);
helps.set(`cd`, [`Cd - type .. to move to parent directory or sub-directory name`]);
helps.set(`md`, [`Md - making directory`]);
helps.set(`mf`, [`Mf - making file`]);
helps.set(`zim`, [`Zim - opening zim redactor`]);

var root = new Dir(``);
	var user = new Dir(`User`);
		var projects = new Dir(`Documents`);
			var game = new Dir(`Game`);
				var main = new File(`main.pj`);

				var src = new Dir(`Src`, true);
					var tips = new Dir(`Tips`);
						var info = new File(`important_info.txt`);

					var draw = new File(`draw.pj`);

					var handler = new File(`handler.pj`);

			var xxx = new Dir(`XXX`);
				var password = new File(`movie.mp4`);
				password.set_file([`Hash of password:`, pass_hash]);

		var music = new Dir(`Music`);
			var shit = new File(`A_peace_of_shit.mp3`);

	var sys = new Dir(`Sys`, true);
		var os = new File(`OS.pkg`);
					tips.add(info);
				src.add(tips);
				src.add(draw);
				src.add(handler);
			game.add(main);
			game.add(src);
			xxx.add(password);
		projects.add(game);
		projects.add(xxx);
		music.add(shit);
	user.add(projects);
	user.add(music);
	sys.add(os);
root.add(user);
root.add(sys);