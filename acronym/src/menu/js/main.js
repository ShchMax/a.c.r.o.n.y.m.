/**
 * Выход из программы
 * 
 * @author ShchMax
 * @file menu
 * @function
 */
function exit_programm() {
	ipcRenderer.send('exit');
}

/**
 * Начало игры
 * 
 * @author ShchMax
 * @file menu
 * @function
 */
function begin_play() {
	ipcRenderer.send('play');
}